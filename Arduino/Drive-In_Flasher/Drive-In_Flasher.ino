void setup() {
  for (int i = 0; i < 5; i++)
  {
    pinMode(i, OUTPUT);
  }
}

void initPins()
{
  for (int i = 0; i < 5; i++)
  {
    digitalWrite(i, LOW);
  }
}

void loop()
{
  for (int i = 0; i < 5; i++)
  {
    digitalWrite(i, HIGH);
    delay(500);
  }
  initPins();
  delay(500);
}
