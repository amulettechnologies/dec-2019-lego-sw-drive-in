# README #

You can use these files to help recreate the scene in this "Christmas Village"
http://www.amulettechnologies.com/holiday-star-wars/

This repository includes a GEMstudio Project, a Arduino project, and 3D models

### How do I get set up? ###

Software Used:
GEMstudio 3.5.4.B or later
Arduino IDE
3D Printer Software

Hardware Used:
Amulet MK-070C-HP
3D Printer with filament
Adafruit Trinket or other tiny Arduino
Ten 5mm LEDs, Current Limiting resistors, Wire, Solder, Soldering Iron, Proto Board, Hot Glue.

